const { Client, RichEmbed } = require('discord.js');
const Datastore = require('nedb');
const client = new Client();
const auth = require('./auth.json');
const package = require('./package.json');
const _f = require('./functions.js');
var args;
var cmd;

// User Variables:
prefix = '!';
adminPrefix = '+';

client.on('ready', () => {
	var logChannel = client.channels.get('535315535433564172');
	// logChannel.send(`[I] Voidbot online.`);
	// logChannel.send(`[V]: Verbose\n[I]: Info\n[E]: Error`);
	
	// if (!channel) return;
	// channel.send("Good morning! I'm awake.");
});

// function kickUnverified() {
// 	var logChannel = client.channels.get('535315535433564172');
// 	try {
// 		if (_f.exVerified === 0) {
// 			logChannel.send(`[V] User did not verify and has been kicked.`);
// 			newUser.send("You have been kicked from the server because you failed to read the rules and verify that you weren't a bot.");
// 			newUser.kick("Did not verify.");
// 			newUser = "";
// 			_f.exVerified = 0;
// 		} else if (_f.exVerified === 1) {
// 			logChannel.send(`[V] User verified and will not be kicked.`);
// 			newUser = "";
// 			_f.exVerified = 0;
// 		}
// 	}
// 	catch(err) {
// 		console.error(`[E] ${err}`);
// 	}
// }

client.on('message', message => {
	var logChannel = client.channels.get('535315535433564172');
	
	//TODO - Make an opt-in for the random messages
	// randNum = Math.floor(Math.random() * 50);
	// if (message.author.id != '513184762073055252') {
	// 	if (randNum === magicNumber) {
	// 		message.channel.send(`I'm sorry ${message.author}, I can't do that.`);
	// 	}
	// }
	try {
		var firstChar = message.content.slice(0,1);
		switch (firstChar) {
			case prefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				logChannel.send(`${_f.botCmd(cmd, args, message)}`);
				break;
			case adminPrefix:
				args = message.content.slice(1).trim().split(/ +/g);
				cmd = args.shift().toLowerCase();
				logChannel.send(`${_f.botCmdAdmin(cmd, args, message)}`);
				break;
		}
	}
	catch (err) {
		console.error(`[E] ${err}`);
	}
});

client.on('guildMemberAdd', member => {
	var logChannel = client.channels.get('535315535433564172');
	var lobbyChannel = client.channels.get('534909532296445971');
	logChannel.send(`[V] New member joined: ${member.displayName}`);
	member.addRole('535257935761244170');
	lobbyChannel.send(`Welcome ${member.displayName}, please read <#534915234830352394>. If you'd like your roles set, please let us know your pronouns and if you'd like access to the NSFW channels, please tell us your age. Also please say "fuck TERFs" if you're not a troll.`);
});

client.login(auth.token);