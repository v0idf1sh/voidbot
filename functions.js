const { Client, RichEmbed } = require('discord.js');
const package = require('/home/abby/voidbot/package.json');
var Datastore = require('nedb');

var help = "Hello, I am Voidbot! Here's a summary of my commands.\n\n!help - Prints this help message.\n!rules - Prints the group rules.\n!ping - Pong!\n!roll X Y - Roll X number of Y-sided dice.\n!random X Y - Generate a random number between X and Y.\n\nVoid Bot maintained by @ralyks#3410 - Version: " + package.version;

function timestamp() {
	date = new Date;
	dateString = date.toISOString();
	return dateString;
}

module.exports = {
	timestamp: function () {
		date = new Date();
		dateString = date.toISOString();
		return dateString;
	},
	botCmd: function (command, arguments, message) {
		switch (command) {
			case 'roll':
				switch (arguments.length) {
					case 0:
						message.channel.send("Roll expects 2 arguments, How many dice and how many sides the dice have.");
						break;
					case 1:
						message.channel.send("Roll expects 2 arguments, How many dice and how many sides the dice have.");
						break;
					case 2:
						totalRoll = 0;
						rollString = "";
						numberOfDice = parseInt(arguments[0]);
						diceSides = parseInt(arguments[1]) + 1; // Adds one to account for Math.random's lack of inclusiveness
						for (i = 0; i < numberOfDice; i++) {
							diceRoll = Math.floor(Math.random() * diceSides);
							rollString = `${rollString}\nDice #${i + 1}: ${diceRoll}`;
							totalRoll = totalRoll + diceRoll;
						}
						message.channel.send(rollString);
						message.channel.send(`Total: ${totalRoll}`);
						break;
				}
				return '[V] Rolled Dice.';
				break;
			case 'random':
				switch (arguments.length) {
					case 0:
						message.channel.send('You must specify a maximum number.');
						break;
					case 1:
						randMax = arguments[0];
						rand = Math.floor(Math.random() * randMax);
						message.channel.send(rand);
						break;
					case 2:
						min = parseInt(arguments[0]);
						max = parseInt(arguments[1]);
						a = max - min + 1;
						b = Math.floor(Math.random() * a);
						c = b + min;
						message.channel.send(c);
						break;
				}
				return "[V] Generated random number.";
				break;
			case 'gif':
				var giphy = require('giphy-api')('B1SX3md8dP9ClBZj0rZo06Bh5zQ8JdQg');
				var searchString = arguments.join(' ');
				try {
					giphy.search(searchString, function (err, res) {
						if (res.data.length >= 5) {
							var rand = Math.floor(Math.random() * 5);
							if (res.data[rand]) {	
								var gifURL = res.data[rand].url;
								var gifEmbedURL = res.data[rand].images.original.url;
								var gifTitle = res.data[rand].title;
			
								const embed = new RichEmbed();
								embed.file = gifEmbedURL;
								embed.title = gifTitle;
			
								message.channel.send(embed);
							} else {
								message.channel.send("No GIF found for "+searchString+".");
							}
						} else {
							var rand = Math.floor(Math.random() * res.data.length);
							if (res.data[rand]) {	
								var gifURL = res.data[rand].url;
								var gifEmbedURL = res.data[rand].images.original.url;
								var gifTitle = res.data[rand].title;
			
								const embed = new RichEmbed();
								embed.file = gifEmbedURL;
								embed.title = gifTitle;
			
								message.channel.send(embed);
							} else {
								message.channel.send("No GIF found for "+searchString+".");
							}
						}
					});
				}
				catch (err) {
					message.channel.send("No GIF found for ",searchString,".");
				}
				return "[V] GIF Requested.";
				break;
			case 'sticker':
				var giphy = require('giphy-api')('B1SX3md8dP9ClBZj0rZo06Bh5zQ8JdQg');
				var searchString = arguments.join(' ');
				try {
					giphy.search({
						api: 'stickers',
						q: searchString
					}, function (err, res) {
						var rand = Math.floor(Math.random() * 5);
						if (res.data[rand]) {
							var stickerURL = res.data[rand].url;
							var stickerEmbedURL = res.data[rand].images.original.url;
							var stickerTitle = res.data[rand].title;
		
							const embed = new RichEmbed();
							embed.file = stickerEmbedURL;
							embed.title = stickerTitle;
		
							message.channel.send(embed);
						} else {
							message.channel.send("No sticker found for "+searchString+".");
						}
					});
				}
				catch (err) {
					message.channel.send("No sticker found for ",searchString,".");
				}
				break;
			case 'help':
				message.channel.send(help);
				return "[V] Help Page Requested.";
				break;
			case 'ping':
				message.channel.send(`Pong!`);
				return "[V] Pinged.";
				break;
			case 'rules':
				return "[V] Rules Requested.";
				break;
			case 'userinfo':
				var userInfo = `Your ID: ${message.author.id}\nYour highestRole ID: ${message.member.highestRole.id}`;
				message.channel.send(userInfo);
				break;
		}
	},
	botCmdAdmin: function (command, arguments, message) {
		switch (command) {
			case 'verify':
			try {
				var mentionedArray = message.mentions.users.array();
				var verifyUser = client.users.get(`${mentionedArray[0].id}`);
				// verifyUser.addRole('535258240498270223');
				// verifyUser.removeRole('535257935761244170');
				return `[V] Verified a user: ${mentionedArray[0].username}#${mentionedArray[0].discriminator}`;
				break;
			}
			catch (err) {
				return "I've failed you Miss Sky. (error)";
			}
		}
	}
}
